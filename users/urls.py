from django.urls import path
from . import views

app_name = 'users'

urlpatterns = [
    path('signin/', views.signUserIn, name="signIn"),
    path('signup/', views.signUserUp, name="signUp"),
    path('activate/<uidb64>/<token>', views.VerificationView.as_view(), name="activate"),
    path('dashboard/', views.getDashboard, name="dashboard"),
    path('profile/<str:username>', views.getProfile, name="profile"),
    path('signout/', views.signUserOut, name="signOut"),
    path('changeProfileData/', views.changeProfileData, name="changeProfileData"),
    path('deleteAccount/', views.deleteAccount, name="deleteAccount")
]
