from django import forms
from django.contrib.auth.models import User
from .models import Profile

class LoginForm(forms.Form):
    username = forms.CharField(label="",widget=forms.TextInput(attrs={'placeholder': 'Nazwa użytkownika', 'class':'textInput'}))
    password = forms.CharField(label="",widget=forms.PasswordInput(attrs={'placeholder': 'Hasło', 'class':'passwordInput'}))

class RegistrationForm(forms.ModelForm):
    password = forms.CharField(label='',widget=forms.PasswordInput(attrs={'placeholder': 'Hasło', 'class':'passwordInput'}))
    password2 = forms.CharField(label='',widget=forms.PasswordInput(attrs={'placeholder': 'Powtórz hasło', 'class':'passwordInput'}))

    class Meta:
        model = User
        fields = ('email', 'username')
        labels = {
            'email': '',
            'username': ''
        }
        widgets = {
            'email': forms.EmailInput(attrs={'placeholder': 'Adres email', 'class':'emailInput'}),
            'username': forms.TextInput(attrs={'placeholder': 'Nazwa użytkownika', 'class': 'textInput'})
        }
    
    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError("Hasła nie są identyczne.")
        return cd['password2']

class EditProfileDataForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('photo','name', 'surname')
        labels = {
            'photo': 'Zdjęcie',
            'name': '', 
            'surname': '',

        }
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Imię', 'class': 'textInput'}),
            'surname': forms.TextInput(attrs={'placeholder': 'Nazwisko', 'class': 'textInput'})
        }
