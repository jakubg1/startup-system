from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage
from .forms import LoginForm, RegistrationForm, EditProfileDataForm
from django.conf import settings
from django.urls import reverse
from django.views import View
from django.utils.http import urlsafe_base64_encode,  urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_str, DjangoUnicodeDecodeError
from django.contrib.sites.shortcuts import get_current_site
from .utils import token_generator
from .models import Profile
from django.contrib.auth.models import User
from django.contrib import messages

# ------------------------LOGIN VIEW -------------------------
def signUserIn(request):
    if request.user.is_authenticated:
        return redirect('users:dashboard')
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request,user)
                    messages.success(request, "Pomyślnie zalogowano.")
                    return redirect('users:dashboard')
        messages.error(request, "Błędny login lub hasło.")

    form = LoginForm()
    return  render(request, 'users/signIn.html', {'form':form})


# ------------------DAHSBOARD VIEW---------------------
@login_required
def getDashboard(request):
    user = request.user
    profile = Profile.objects.get(user=user)
    return render(request, 'users/dashboard.html', {'user':user,"profile":profile})


# -----------------------LOGOUT VIEW--------------------
@login_required
def signUserOut(request):
    logout(request)
    messages.success(request, "Pomyślnie wylogowano.")
    return redirect('users:signUp')


# ------------------REGISTER VIEW--------------------
def signUserUp(request):
    if request.user.is_authenticated:
        return redirect('users:dashboard')
    if request.method == 'POST':
        user_form = RegistrationForm(request.POST)
        if user_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user_form.cleaned_data['password'])
            user.is_active = False
            user.save()
            profile = Profile.objects.create(user=user)
            uidb64 = urlsafe_base64_encode(force_bytes(user.pk))
            domain = get_current_site(request).domain
            link = reverse('users:activate',kwargs={'uidb64':uidb64, 'token':token_generator.make_token(user)})
            url_activate = 'http://'+domain+link
            email_body = 'Witaj ' + user.username + ', kliknij w link, aby aktywować swoje konto: '+url_activate
            mail = EmailMessage(
                "Link aktywacyjny do konta na strup",
                email_body,
                settings.EMAIL_HOST_USER,
                (user.email,)
            )
            mail.send()
            messages.success(request, "Gratulacje, proces rejestracji przeszedł pomyślnie. Potwierdź teraz swoje konto, klikając w link aktywacyjny, który dostarczyliśmy Ci pocztą na wskazany przez Ciebie adres e-mail.")
            return redirect('users:dashboard')
        else:
            messages.error(request, "Nieprawidłowe dane rejestracji.")
    else:
        user_form = RegistrationForm()
    return render(request, 'users/signUp.html', {'user_form':user_form})


# -----------------------ACTIVATE USERS --------------------------------
class VerificationView(View):
    def get(self, request, uidb64, token):
        try:
            uid = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist, DjangoUnicodeDecodeError):
            user = None
        if user is not None and token_generator.check_token(user, token):
            user.is_active = True
            user.save()
            messages.success(request, "Weryfikacja zakończona. Teraz możesz się zalogować.")
        else:
            messages.error(request, "Błędne dane weryfikacji.")
        return redirect('users:signIn')


# -----------------------DELETE USER --------------------------------
@login_required
def deleteAccount(request):
    user = User.objects.get(pk=request.user.pk)
    logout(request)
    user.delete()
    messages.success(request, "Konto zostało pomyślnie usunięte. ")
    return redirect('users:signUp')


# -----------------------CHANGE USER IMAGE --------------------------------
@login_required
def changeProfileData(request):
    if request.method == 'POST':
        form = EditProfileDataForm(instance=request.user.profile, data=request.POST, files=request.FILES)
        if form.is_valid():
            profile = form.save(commit=False)
            profile.user = request.user
            profile.save()
            messages.success(request, "Dane zostały pomyślnie zmienione.")
        else:
            messages.error(request, "Nieprawidłowe dane.")
        return redirect('users:dashboard')
    form = EditProfileDataForm(instance=request.user.profile)
    return render(request, 'users/changeProfileData.html', {'form':form})


# -----------------------USER PROFILE --------------------------------
def getProfile(request, username):
    user = User.objects.get(username=username)
    if user == request.user:
        return redirect('users:dashboard')
    profile = Profile.objects.get(user=user)
    return render(request, 'users/profile.html', {'user':user,"profile":profile})

