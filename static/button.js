let navBtn = document.getElementById('menuBtn')


let closeMenuBtn = document.querySelector('#closeMenuBtn')

function openMobileMenu(){
	let tlopen = gsap.timeline();
	tlopen.addLabel('start')
	.to('#mobileMenu', 0.6,{x: '0',ease: 'expo.out'})
	.to('.mobileNavItem', {delay: 0.1,duration: 0.15, ease: 'expo.out', stagger:0.1, x: '0'}, "start")
}

function closeMobileMenu(){
	let tlclose = gsap.timeline();
	tlclose.addLabel('start')
	.to('.mobileNavItem', {duration: 0.08, ease: 'expo.out', stagger: -0.1, x: '+=250px'})
	.to('#mobileMenu',0.6,{x: '+=100%',ease: 'expo.out'})
}


navBtn.addEventListener('click', openMobileMenu)
closeMenuBtn.addEventListener('click', closeMobileMenu)