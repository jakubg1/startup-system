from django.contrib import admin
from .models import Startup, Observation, StartupImage

@admin.register(Startup)
class StartupAdmin(admin.ModelAdmin):
    list_display = ('title', 'shortDescription', 'author')
    
@admin.register(Observation)
class ObservationAdmin(admin.ModelAdmin):
    list_display = ('observator', 'startup')


@admin.register(StartupImage)
class StartupImageAdmin(admin.ModelAdmin):
    list_display = ('photo','startup')
    