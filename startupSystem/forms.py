from django import forms
from . import models

class CreateStartupForm(forms.ModelForm):
    class Meta:
        model = models.Startup
        fields = ['title','shortDescription', 'mainPhoto']
        labels = {
            'title': '',
            'shortDescription': '',
            'mainPhoto': 'Zdjęcie główne'
        }
        widgets = {
            'shortDescription': forms.Textarea(attrs={'placeholder': 'Opis startupu ', 'class':'textArea'}),
            'title': forms.TextInput(attrs={'placeholder': 'Nazwa startupu ', 'class': 'textInput'})
        }

class AddImageForm(forms.ModelForm):
    class Meta:
        model = models.StartupImage
        fields = ['photo']
        labels = {
            'photo': 'Zdjęcie'
        }