from django.urls import path
from . import views

app_name = 'startupsSystem'

urlpatterns = [
    path('', views.startupsList, name="startupsList"),
    path('mystartups/', views.userStartups, name="myStartups"),
    path('authorStartups/<str:username>/', views.authorStartups, name="authorStartups"),
    path('startup/<int:pk>/', views.startupDetails, name="startupDetails"),
    path('startup/observe/<int:pk>/', views.observeStartup, name="observeStartup"),
    path('startup/edit/<int:pk>/', views.editStartup, name="editStartup"),
    path('startup/unobserve/<int:pk>/', views.unobserveStartup, name="unobserveStartup"),
    path('startup/observations/<int:pk>/', views.startupObservations, name="startupObservations"),
    path('startup/delete/<int:pk>/', views.startupDelete, name="startupDelete"),
    path('createStartup/', views.createStartup, name="createStartup"),
    path('myobservations/', views.myObservations, name="myObservations"),
    path('addPictureToStartup/<int:pk>/',views.addPictureToStartup, name="addPictureToStartup"),
]


