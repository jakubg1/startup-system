from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

class Startup(models.Model):
    author = models.ForeignKey(User, related_name="startups", on_delete=models.CASCADE)
    title = models.CharField(max_length=500)
    shortDescription = models.CharField(max_length=8000)
    createDate = models.DateField(auto_now_add=True)
    mainPhoto = models.ImageField(upload_to='startupPictures/main', blank=True)

    def url(self):
        return reverse('startupsSystem:startupDetails',args=[self.pk])

    def url_edit(self):
        return reverse('startupsSystem:editStartup',args=[self.pk])

    def url_delete(self):
        return reverse('startupsSystem:startupDelete',args=[self.pk])

    def url_observe(self):
        return reverse('startupsSystem:observeStartup',args=[self.pk])

    def url_unobserve(self):
        return reverse('startupsSystem:unobserveStartup',args=[self.pk])

    def url_observations(self):
        return reverse('startupsSystem:startupObservations',args=[self.pk])

    def url_add_image(self):
        return reverse('startupsSystem:addPictureToStartup', args=[self.pk])


class Observation(models.Model):
    observator = models.ForeignKey(User, related_name="observations", on_delete=models.CASCADE)
    startup = models.ForeignKey(Startup, related_name="startups", on_delete=models.CASCADE)
    createDate = models.DateField(auto_now_add=True)


class StartupImage(models.Model):
    startup = models.ForeignKey(Startup, related_name="images", on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='startupPictures')
