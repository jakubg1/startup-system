from django.shortcuts import render, get_object_or_404, redirect, HttpResponse
from django.contrib.auth.decorators import login_required
from .forms import CreateStartupForm, AddImageForm
from .models import Startup, Observation, StartupImage
from django.contrib.auth.models import User
from django.contrib import messages

def startupsList(request):
    startups = Startup.objects.order_by('-id')
    return render(request, 'startupsSystem/listStartups.html',{'startups': startups})

@login_required
def userStartups(request):
    startups = Startup.objects.filter(author=request.user)
    return render(request, 'startupsSystem/myStartups.html',{'startups': startups})

def startupDetails(request, pk):
    startup = get_object_or_404(Startup, pk=pk)
    images = StartupImage.objects.filter(startup=startup)
    canObserve = True
    if request.user.is_authenticated:
        if Observation.objects.filter(startup=startup, observator=request.user).exists() or startup.author == request.user:
            canObserve = False
    return render(request, 'startupsSystem/startupDetails.html',{'startup': startup, 'canObserve':canObserve, "images":images})

@login_required
def startupDelete(request, pk):
    startup = get_object_or_404(Startup, pk=pk, author=request.user)
    startup.delete()
    messages.success(request,"Pomyślnie usunięto startup.")
    return redirect('startupsSystem:myStartups')

@login_required
def createStartup(request):
    if request.method == 'POST':
        form = CreateStartupForm(request.POST, request.FILES)
        if form.is_valid():
            startup = form.save(commit=False)
            startup.author = request.user
            startup.save()
            messages.success(request, "Startup został pomyślnie utworzony.")
            return redirect('startupsSystem:myStartups')
        messages.error(request, "Nieprawidłowe dane.")
    form = CreateStartupForm()
    return render(request, 'startupsSystem/createStartup.html', {'form': form})
        
@login_required
def observeStartup(request, pk):
    user = request.user
    startup = get_object_or_404(Startup, pk=pk)
    if not Observation.objects.filter(startup=startup, observator=user).exists() and user != startup.author:
        observation = Observation.objects.create(observator = user, startup=startup)
        messages.success(request, "Pomyślnie zaobserwowano startup.")
    else:
        messages.error(request, "Nieprawidłowe dane.")
    return redirect('startupsSystem:startupDetails', pk)

@login_required
def unobserveStartup(request, pk):
    user = request.user
    startup = get_object_or_404(Startup, pk=pk)
    if Observation.objects.filter(startup=startup, observator=user).exists():
        observation = Observation.objects.filter(observator = request.user, startup=startup).delete()
        messages.success(request, "Startup przestał być przez Ciebie obserwowany.")
    else:
        messages.error(request, "Nieprawidłowe dane.")
    return redirect('startupsSystem:startupDetails', pk)

@login_required
def startupObservations(request, pk):
    startup = get_object_or_404(Startup, pk=pk)
    observations = Observation.objects.filter(startup=startup)
    return render(request, 'startupsSystem/startupObservations.html',{"observations":observations})

@login_required
def myObservations(request):
    user = request.user
    observations = Observation.objects.filter(observator=user)
    return render(request, 'startupsSystem/myObservations.html',{"observations":observations})

@login_required
def addPictureToStartup(request, pk):
    if request.method == 'POST':
        form = AddImageForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            user = request.user
            startup = get_object_or_404(Startup, pk=pk, author=user)
            img = form.save(commit=False)
            img.startup = startup
            img.save()
            messages.success(request, "Pomyślnie dodano zdjęcie do startupu.")
        else:
            messages.error(request, "Nieprawidłowe dane.")
        return redirect('startupsSystem:startupDetails', pk)
    form = AddImageForm()
    return render(request, 'startupsSystem/addPictureToStartup.html', {'form':form})


def authorStartups(request, username):
    user = User.objects.get(username=username)
    startups = Startup.objects.filter(author=user)
    return render(request, 'startupsSystem/authorStartups.html', {'startups':startups, 'user':user})


@login_required
def editStartup(request, pk):
    user = request.user
    startup = get_object_or_404(Startup, pk=pk, author=user)
    if request.method == 'POST':
        form = CreateStartupForm(instance=startup,data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Pomyślnie wykonano edycję startupu.")
        else:
            messages.error(request, "Nieprawidłowe dane.")
        return redirect('startupsSystem:startupDetails', pk)

    form = CreateStartupForm(instance=startup)
    return render(request, 'startupsSystem/editStartup.html', {'form':form})