# Generated by Django 3.1 on 2022-05-30 20:32

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('startupSystem', '0002_auto_20220529_2113'),
    ]

    operations = [
        migrations.AddField(
            model_name='startup',
            name='createDate',
            field=models.DateField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
