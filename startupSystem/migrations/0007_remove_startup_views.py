# Generated by Django 4.0.5 on 2022-06-10 21:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('startupSystem', '0006_rename_author_observation_observator'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='startup',
            name='views',
        ),
    ]
