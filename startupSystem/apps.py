from django.apps import AppConfig


class StartupsystemConfig(AppConfig):
    name = 'startupSystem'
